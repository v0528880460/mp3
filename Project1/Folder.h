#pragma once

#include "Song.h"
#include "Collection.h"
#include <list>
#include <string>

using std::list;
using std::string;
 
class Folder {
	string namefolder;
	Collection<Folder> folders;
	list<Song> songs;
	Folder* Foldersearch(string name);
	bool folderexist(string name);
	bool songexist(string name);

public:

	Folder(string name) : namefolder(name), folders(), songs() { 

	}

	void AddFolder(string name);
	void AddFolder(string name, string parent);

	void AddSong(string name, string namesinger, string words);
	void AddSong(string name, string namesinger, string words, string parent);
	void RemoveSong(string name);
	void RemoveSong(string name,string parent);
	void RemoveFolder(string name);
	void PrintSongs();
	void PrintSongs(string singer);
	void Play(string name);
	void Play(string name, string parent);
	void MoveSong(string name, string parent);
	void MoveSong(string name, string current, string parent);
	void PrintFolderSongs(string parent);
	void PrintFolderSongs(string parent, string singer);

	bool operator==(const Folder& other) const {
		return namefolder == other.namefolder;
	}

	bool operator>(const Folder& other) const {
		return namefolder > other.namefolder;
	}
}; 