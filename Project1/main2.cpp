#include "Collection.h" 
#include <string> 
#include <iostream>

using std::string;
using std::cout;
using std::endl;

int main_()
{
	Collection<string> stringCollection;
	stringCollection.Add("Hello");
	stringCollection.Add("World");
	Iterator<string> itr = stringCollection.GetIterator();
	while (itr.HasNext())
	{
		cout << itr.Next() << endl;
	}
	itr = stringCollection.GetIterator();
	while (itr.HasNext())
	{
		const string& s = itr.Next();
		if (s == "Hello")
		{
			cout << s << endl;
		}
		else
		{
			itr.Remove();
		}
	}
	cout << "there are " << stringCollection.Size() << " elements in the collection. " << endl; 
		itr = stringCollection.GetIterator();
	while (itr.HasNext())
	{
		cout << itr.Next() << endl;
	}
	stringCollection.Add("A");
	stringCollection.Add("B");
	stringCollection.Add("a");
	stringCollection.Add("b");
	stringCollection.Add("shalom");
	cout << "there are " << stringCollection.Size() << " elements in the collection. " << endl; 
		itr = stringCollection.GetIterator();
	while (itr.HasNext())
	{
		cout << "removing " << itr.Next() << endl;
		itr.Remove();
	}
	if (!itr.HasNext())
	{
		cout << "No more elements!" << endl;
	}
	return 0;
}