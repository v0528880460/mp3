#include <iostream>
#include <string>
//#include <windows.h>
//#include "Collection.h"
#include "MySongs.h"

using std::string;
using std::cout;
//using std::endl;

 

int main()
{
	//int d;
	//cout << d;
	//Song a("jjj","hhhh", "hhh");
	//printf

	/*Collection<A> col;
	col.Add(A());*/

	try {
		MySongs mySongs;

		mySongs.AddSong("MATAM EX4", "Ari", "We like C++ programing...");
		mySongs.AddFolder("Hebrew");
		mySongs.AddFolder("old", "Hebrew");
		mySongs.AddFolder("new", "Hebrew");

		mySongs.AddSong("Road of the King", "Gili", "Road of the king is my only route...", "old");
		// mySongs.RemoveSong("MATAM EX4");
		mySongs.PrintSongs();
		mySongs.AddSong("Road of the King", "Prison 6", "Road of the king is my only route...", "new");
		mySongs.PrintSongs();
		mySongs.Play("MATAM EX4");
		mySongs.MoveSong("MATAM EX4", "Hebrew");
		mySongs.PrintFolderSongs("Hebrew");
		mySongs.MoveSong("MATAM EX4", "new", "Hebrew");
		mySongs.Play("MATAM EX4", "Hebrew"); // This will fail! 
		mySongs.PrintSongs("Shlomo"); // Will print nothing. 
		mySongs.PrintFolderSongs("new", "Prison 6");
		mySongs.RemoveSong("Road of the King"); // This will fail! 
		mySongs.RemoveSong("Road of the King", "old");
		mySongs.RemoveFolder("old");
	}
	catch (std::exception& e) {
		std::cout << "EXCEPTION" << std::endl;
	}

	return 0;
}