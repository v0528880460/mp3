#pragma once

#include <string> 
#include <iostream> 

using std::string;

template<class T>
class Collection;

template<class T>
class Node;


template<class T>
class Iterator 
{
	Node<T>* current;
	Collection<T>* col;
	bool first;

	Iterator(Node<T>* current, Collection<T>* col) : current(current), col(col), first(true)
	{}

	friend class Collection<T>;

public:
	T& Next() {
		if (first) {
			first = false;
			return current->item;
		}

		current = current->next;
		return current->item;
	}

	bool HasNext() const {
		if (current == NULL) {
			return false;
		}

		if (!first && current->next == NULL) {
			return false;
		}

		return true;
	}

	void Remove() {
		Node<T>* next = current->next;
		col->Remove(current);
		current = next;
	}
};

template<class T>
class Node {
	T item;
	Node<T>* next;

	Node(T item) : item(item), next(NULL)
	{}
	
	friend class Collection<T>;
	friend class Iterator<T>;
};

template<class T>
class Collection {
	Node<T>* first;

public:
	Collection() : first(NULL)
	{}

	~Collection() {
		while (first != NULL) {
			Node<T>* temp = first->next;
			delete first;
			first = temp;
		}
	}

	void Add(T item) {
		Node<T>* n = new Node<T>(item);

		if (first == NULL || first->item > item) {
			n->next = first;
			first = n;
			return;
		}

		Node<T>* current = first;

		while (current->next != NULL && item > current->next->item) {
			current = current->next;
		}

		n->next = current->next;
		current->next = n;
	}

	int Size() {
		int s = 0;
		Node<T>* current = first;
		while (current != NULL) {
			current = current->next;
			s++;
		}
		return s;
	}
 
	Iterator<T> GetIterator() {
		return Iterator<T>(first, this);
	}

	void Remove(Node<T>* toremove) {
		if (first == toremove) {
			Node<T>* temp = first;
			first = first->next;
			delete temp;
			return;
		}

		Node<T>* current = first;
		while (current->next != toremove) {
			current = current->next;
		}
		Node<T>* temp = current->next;
		current->next = current->next->next;
		delete temp;
	}
};
