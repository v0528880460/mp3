#pragma once
#include <iostream>
#include <string>

using std::string;

class Song {
	string namesong;
	string namesinger;
	string words;

public: 
	Song(string namesong, string namesinger, string words) :
		namesong(namesong), namesinger(namesinger), words(words) {

	}

	string getname() const {
		return namesong;
	}

	string getnamesinger() const {
		return namesinger;
	}

	string getwords() const {
		return words;
	}

	bool operator==(const Song& other) const {
		return namesong == other.namesong;
	}
}; 