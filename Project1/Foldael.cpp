#include "Folderl.h"

Folder* Folder::Foldersearch(string name) {
	if (namefolder == name) {
		return this;
	}

	for (Folder& f : folders) {
		if (f.namefolder == name) {
			return &f;
		}

		Folder* temp = f.Foldersearch(name);
		if (temp != NULL) {
			return temp;
		}
	}

	return NULL;
}

bool Folder::folderexist(string name) {
	if (name == namefolder) {
		return true;
	}

	for (Folder& f : folders) {
		if (f.folderexist(name)) {
			return true;
		}
	}

	return false;
}

bool Folder::songexist(string name) {


	for (Song& song : songs) {
		if (song.getname() == name) {
			return true;
		}
	}

	for (Folder& f : folders) {
		if (f.songexist(name)) {
			return true;
		}
	}

	return false;
}

void Folder::AddFolder(string name) {
	Folder temp(name);
	folders.push_front(temp);
	// folders.push_front(Folder(name));
}

void Folder::AddFolder(string name, string parent) {
	if (folderexist(name) || !folderexist(parent)) {
		throw std::exception();
	}

	Foldersearch(parent)->AddFolder(name);
}

void Folder::AddSong(string name, string namesinger, string words) {

	if (songexist(name)) {
		throw std::exception();
	}


	Song song(name, namesinger, words);
	songs.push_back(song);
}

void Folder::AddSong(string name, string namesinger, string words, string parent) {
	if (!folderexist(parent)) {
		//throw std::exception();
		return;
	}

	if (namefolder == parent) {
		AddSong(name, namesinger, words);
		return;
	}

	for (Folder& f : folders) {
		f.AddSong(name, namesinger, words, parent);
	}
}

void Folder::RemoveSong(string name) {
	if (!songexist(name)) {
		throw std::exception();
	}

	for (Song& song : songs) {
		if (song.getname() == name) {
			songs.remove(song);
			break;
		}
	}
}
void Folder::RemoveSong(string name, string parent) {
	if (!folderexist(parent)) {
		throw std::exception();
	}

	Foldersearch(parent)->RemoveSong(name);

	/*if (namefolder == parent) {
		RemoveSong(name );
		return;
	}

	for (Folder& f : folders) {
		f.RemoveSong(name, parent);
	}*/
}

void Folder::PrintSongs() {
	for (Song& song : songs) {
		std::cout << song.getname() << "\n";
	}

	for (Folder& folder : folders) {
		folder.PrintSongs();
	}
}

void Folder::PrintSongs(string singer) {
	for (Song& song : songs) {
		if (song.getnamesinger() == singer) {
			std::cout << song.getname() << "\n";
		}
	}

	for (Folder& folder : folders) {
		folder.PrintSongs(singer);
	}
}

void Folder::PrintFolderSongs(string parent) {
	for (Song& song : Foldersearch(parent)->songs) {
		std::cout << song.getname() << "\n";
	}
}

void Folder::PrintFolderSongs(string parent, string singer) {
	for (Song& song : Foldersearch(parent)->songs) {
		if (song.getnamesinger() == singer) {
			std::cout << song.getname() << "\n";
		}
	}
}

void Folder::RemoveFolder(string name) {
	if (namefolder == name) {
		throw std::exception();
		/*for (Folder& f : folders) {
			f.RemoveSong(name, f.namefolder);
			folders.remove(f);
		}		return;*/
	}

	for (Folder& f : folders) {
		if (f.namefolder == name) {
			folders.remove(f);
			return;
		}
		f.RemoveFolder(name);
	}
}
void Folder::MoveSong(string name, string parent) {
	for (Song& song : songs) {
		if (song.getname() == name) {
			Foldersearch(parent)->AddSong(song.getname(), song.getnamesinger(), song.getwords());
			songs.remove(song);
			break;
		}
	}


}

void Folder::MoveSong(string name, string current, string parent) {
	Folder* currentparent = Foldersearch(current);
	for (Song& song : currentparent->songs) {
		if (song.getname() == name) {
			Foldersearch(parent)->AddSong(song.getname(), song.getnamesinger(), song.getwords());
			currentparent->songs.remove(song);
			break;
		}
	}
}

void Folder::Play(string name) {
	for (Song& song : songs) {
		if (song.getname() == name) {
			std::cout << song.getwords() << "\n";
			break;
		}
	}
}

void Folder::Play(string name, string parent) {
	Foldersearch(parent)->Play(name);
}
